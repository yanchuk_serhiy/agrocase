<!DOCTYPE html>
<html lang="uk">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <title>
        Agrocase - 1C ERP - система управління агровиробничим підприємством по
        вирощуванні та переробці
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="img/thumbnail.png" type="image/x-icon" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <!--
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    -->

    <link
            rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
            integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
            crossorigin="anonymous"
    />
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="/css/owl.carousel.min.css" />
    <!-- Owl Carousel Theme -->
    <link rel="stylesheet" href="/css/owl.theme.default.min.css" />
    <!-- Lity -->
    <link rel="stylesheet" href="/css/lity.min.css" />
    <!-- Anomations -->
    <link rel="stylesheet" href="/css/animations.css" />
    <!-- Style -->
    <link rel="stylesheet" href="/css/style.css" />
    <!-- Media Queries -->
    <link rel="stylesheet" href="/css/media-queries.css" />
    <!-- Color Theme -->
    <!-- <link rel="stylesheet" href="css/color-theme/color-theme-1.css"> -->
    <!-- <link rel="stylesheet" href="css/color-theme/color-theme-2.css"> -->
    <!-- <link rel="stylesheet" href="css/color-theme/color-theme-3.css"> -->
    <!-- <link rel="stylesheet" href="css/color-theme/color-theme-4.css"> -->
    <link rel="stylesheet" href="/css/color-theme/color-theme-5.css" />
    <!-- Switch Style -->
    <link id="switch_style" href="/css/color-theme/" rel="stylesheet" />

    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css"
    />

    <link
            rel="stylesheet"
            href=" https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css"
    />

</head>

<body>

<!-- Modal PRICE -->
<div
        class="modal fade"
        id="success-modal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Вашу заявку прийнято!</h5>
                <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Ми зв'яжемось з вами найблищим часом</p>
                <button data-dismiss="modal" class="submit submit-modal bg-color-1">
                    ОК
                </button>
            </div>
        </div>
    </div>
</div>



<!-- Modal PRICE -->
<div
        class="modal fade"
        id="for-cost-modal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Заявка на вартість</h5>
                <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="cost-form" action="">
                        <div class="col-sm-12 col-md-12">
                            <input id="cost-name" type="text" name="name" placeholder="Імя *" />
                        </div>
                        <div class="row row-modal">
                            <div class="col-sm-12 col-md-6">
                                <input id="cost-phone" class="phone-valid" type="text" name="phone" placeholder="Телефон *"/>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input id="cost-email" type="text" name="email" placeholder="Електронна адреса *"/>
                            </div>
                        </div>
                        <div class="row row-modal">
                            <div class="col-sm-12 col-md-6">
                                <input id="cost-company" type="text" name="company-name" placeholder="Компанія *"/>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input id="cost-user-count"class="user-count" type="text" name="user-count" placeholder="Кількість користувачів"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="select">
                                <select name="slct" class="custom-sel" id="cost-oblast">
                                    <option>Область</option>
                                    <option value="Івано-Франківська область">Івано-Франківська область</option>
                                    <option value="Вінницька область">Вінницька область</option>
                                    <option value="Волинська область">Волинська область</option>
                                    <option value="Дніпропетровська область">Дніпропетровська область</option>
                                    <option value="Донецька область">Донецька область</option>
                                    <option value="Житомирська область">Житомирська область</option>
                                    <option value="Закарпатська область">Закарпатська область</option>
                                    <option value="Запорізька область">Запорізька область</option>
                                    <option value="Кіровоградська область">Кіровоградська область</option>
                                    <option value="Київська область">Київська область</option>
                                    <option value="Луганська область">Луганська область</option>
                                    <option value="Львівська область">Львівська область</option>
                                    <option value="Миколаївська область">Миколаївська область</option>
                                    <option value="Одеська область">Одеська область</option>
                                    <option value="Полтавська область">Полтавська область</option>
                                    <option value="Рівненська область">Рівненська область</option>
                                    <option value="Сумська область">Сумська область</option>
                                    <option value="Тернопільська область">Тернопільська область</option>
                                    <option value="Харківська область">Харківська область</option>
                                    <option value="Херсонська область">Херсонська область</option>
                                    <option value="Хмельницька область">Хмельницька область</option>
                                    <option value="Черкаська область">Черкаська область</option>
                                    <option value="Чернівецька область">Чернівецька область</option>
                                    <option value="Чернігівська область">Чернігівська область</option>
                                    <option value="АР Крим">АР Крим</option>
                                </select>
                            </div>
                        </div>

                        <!-- <input type="text" name="name" placeholder="Name" /> -->

                        <div class="col-md-12">
                            <textarea id="cost-messege" name="massege"  placeholder="Вид діяльності компанії та блоки які вас цікавлять"></textarea>
                        </div>
                        <div class="col-md-12">
                            <button id="cost-submit" class="submit submit-modal bg-color-1">
                                Відправити <i class="fa fa-paper-plane" aria-hidden="true"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal present -->
<div
        class="modal fade"
        id="for-present-modal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Заявка на презентацію</h5>
                <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="present-form" action="">
                        <div class="col-sm-12 col-md-12">
                            <input id="present-name" type="text" name="name" placeholder="Імя *" />
                        </div>
                        <div class="row row-modal">
                            <div class="col-sm-12 col-md-6">
                                <input id="present-phone" class="phone-valid" type="text" name="phone" placeholder="Телефон *"/>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input id="present-email" type="text" name="email" placeholder="Електронна адреса *"/>
                            </div>
                        </div>
                        <div class="row row-modal">
                            <div class="col-sm-12 col-md-12">
                                <input id="present-company" type="text" name="company" placeholder="Компанія *"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="select">
                                <select name="region" class="custom-sel-present" id="present-oblast">
                                    <option>Область</option>
                                    <option value="Івано-Франківська область">Івано-Франківська область</option>
                                    <option value="Вінницька область">Вінницька область</option>
                                    <option value="Волинська область">Волинська область</option>
                                    <option value="Дніпропетровська область">Дніпропетровська область</option>
                                    <option value="Донецька область">Донецька область</option>
                                    <option value="Житомирська область">Житомирська область</option>
                                    <option value="Закарпатська область">Закарпатська область</option>
                                    <option value="Запорізька область">Запорізька область</option>
                                    <option value="Кіровоградська область">Кіровоградська область</option>
                                    <option value="Київська область">Київська область</option>
                                    <option value="Луганська область">Луганська область</option>
                                    <option value="Львівська область">Львівська область</option>
                                    <option value="Миколаївська область">Миколаївська область</option>
                                    <option value="Одеська область">Одеська область</option>
                                    <option value="Полтавська область">Полтавська область</option>
                                    <option value="Рівненська область">Рівненська область</option>
                                    <option value="Сумська область">Сумська область</option>
                                    <option value="Тернопільська область">Тернопільська область</option>
                                    <option value="Харківська область">Харківська область</option>
                                    <option value="Херсонська область">Херсонська область</option>
                                    <option value="Хмельницька область">Хмельницька область</option>
                                    <option value="Черкаська область">Черкаська область</option>
                                    <option value="Чернівецька область">Чернівецька область</option>
                                    <option value="Чернігівська область">Чернігівська область</option>
                                    <option value="АР Крим">АР Крим</option>
                                </select>
                            </div>
                        </div>

                        <!-- <input type="text" name="name" placeholder="Name" /> -->

                        <div class="col-md-12">
                            <textarea id="present-messege" name="massege"  placeholder="Вид діяльності компанії та блоки які вас цікавлять"></textarea>
                        </div>
                        <div class="col-md-12">
                            <button id="present-submit" class="submit submit-modal bg-color-1">
                                Відправити <i class="fa fa-paper-plane" aria-hidden="true"></i>
                            </button>                  </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Preloader -->
<div class="preloader">
    <div class="status">
        <div class="spinner">
            <div class="bg-color-2 rect1"></div>
            <div class="bg-color-2 rect2"></div>
            <div class="bg-color-2 rect3"></div>
            <div class="bg-color-2 rect4"></div>
            <div class="bg-color-2 rect5"></div>
        </div>
    </div>
</div>
<!-- Preloader end -->
<!-- Page -->
<div class="page-wrap header-bg">
    <!-- Color Switcher -->
    <div id="color-sw" class="color-switcher">
        <div class="choose-color">
            <div class="box" id="color-theme-1"></div>
            <div class="box" id="color-theme-2"></div>
            <div class="box" id="color-theme-3"></div>
            <div class="box" id="color-theme-4"></div>
            <div class="box" id="color-theme-5"></div>
        </div>
        <!--
          <div class="bg-color-1 cog"><i class="play-rotate fas fa-cog"></i></div>
        -->
    </div>
    <!-- Color Switcher end -->
    <!-- Navigation -->
    <nav id="navigation" class="navbar navbar-expand-lg">
        <div class="container">
            <!-- Logo -->
            <a class="navbar-brand" href="#">
                <!-- <div class="logo bg-color-1">A</div> -->
                <img class="logo-img" src="img/agrocase-logo.png" />
            </a>
            <!-- Logo end -->
            <!-- Burger menu button -->
            <button
                    class="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
            >
                <span class="burger-menu"> <span class="burger"></span> </span>
            </button>
            <!-- Burger menu button end -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <!-- Navigation Link 1 -->
                    <!--
                      <li class="nav-item active">
                          <a class="nav-link mx-auto" href="#top">Оперативний облік</a>
                      </li>
                    -->
                    <li class="nav-item ">
                        <a class="nav-link" href="#top">Оперативний облік</a>
                    </li>
                    <!-- Navigation Link 2 -->
                    <li class="nav-item">
                        <a class="nav-link" href="#hiw">Моб. додаток</a>
                    </li>
                    <!-- Navigation Link 3 -->
                    <!-- <li class="nav-item">
                      <a class="nav-link" href="#about">Картографія</a>
                    </li> -->

                    <!-- Navigation Link 5 -->
                    <li class="nav-item">
                        <a class="nav-link" href="#price">Фін. результат </a>
                    </li>
                    <!-- Navigation Link 4 -->
                    <li class="nav-item">
                        <a class="nav-link" href="#testimonials">Аналітика (CEO-BI)</a>
                    </li>
                    <!-- Navigation Link 6 -->
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Архітектура системи</a>
                    </li>
                    <!--
                      <li class="nav-item button header-button bg-color-1">
                          <a class="nav-link" href="#"><i class="fas fa-cart-arrow-down"></i> Purchase now</a>
                      </li>
                    -->
                </ul>
            </div>
        </div>
    </nav>
    <!-- Navigation end -->
    <!-- Header -->
    <header class="header">
        <div class="container">
            <div class="row header-row">
                <!-- Main title -->
                <div class="col-sm-12 col-md-6 align-self-center main-title">
                    <p class="light bold small main-suptitle">
                        Вирощування та переробка
                    </p>
                    <h1 class="home-title">
                        <b>AGROCASE:</b
                        ><span class="thin">
                  1C ERP - система управління агровиробничим підприємством</span
                        >
                    </h1>

                    <!-- Buttons -->
                    <a href="#." data-toggle="modal" data-target="#for-cost-modal">
                        <div class="button bg-color-1 cost-btn ">Заявка на вартість</div>
                    </a>
                    <a
                            class="gray link present-btn"
                            href="#."
                            data-toggle="modal"
                            data-target="#for-present-modal"
                    >Заявка на презентацію <i class="fas fa-chevron-right"></i
                        ></a>
                </div>
                <!-- Main title end -->
                <!-- Phone -->
                <div class="col-12 col-md-6 align-self-center header-macbook home-mackbook jump">
                    <img src="img/macbook.png" alt="macbook" />
                    <div class="home-screen">
                        <img src="img/general.png" alt="">
                    </div>

                    <a
                            href="https://www.youtube.com/watch?v=i4klV22IAhk"
                            data-lity=""
                    >
                        <div class="play-button btn--shockwave is-active bg-color-1 home-play-btn">
                            <i class="fas fa-play"></i>
                        </div>
                    </a>
                </div>
                <!-- Phone end -->
            </div>
            <!-- Tooltip -->
            <a>
                <div class="header-tooltip">
                    <i class="fas scroll-arrow bounce fa-angle-double-down"></i>
                    <p class="light thin small">
                        Прокрутіть вниз<br />Щоб дізнатися більше
                    </p>
                </div>
            </a>
            <!-- Tooltip end -->
        </div>
    </header>
    <!-- Header end -->
    <!-- How it works -->
    <section class="hiw p-90-60 pb-0">
        <div class="line"></div>
        <div class="container">
            <!-- Section title -->
            <div class="section-title sm-ac mb-90 ">
                <!--
                  <p class="light thin">&nbsp;</p><p class="light thin">&nbsp;</p>
                -->

                <p class="light bold small" style="margin-top: 20px">
                    Що Ви отримаєте
                </p>
                <h2><span class="thin">Ефективний</span> <b>Результат</b></h2>
            </div>
            <!-- Section title end -->
            <div class="row">
                <!-- Item 1 -->
                <div class="col-6 col-sm-6 col-md-3 hiw-item mb-30">
                    <div class="bg-color-2 puls mb-30" style="animation-delay: 0s">
                        <i class="fas fa-file-contract"></i>
                    </div>
                    <p class="bold">Ефективне використання ресурсів</p>
                    <!-- <p
                      class="light thin"
                      style="font-weight: 100; font-family: roboto;"
                    >
                      Короткий опис, який додатково розписує пункт в два рядки.
                    </p> -->
                </div>
                <!-- Item 2 -->
                <div class="col-6 col-sm-6 col-md-3 hiw-item mb-30">
                    <div class="bg-color-2 puls mb-30" style="animation-delay: .75s">
                        <i class="fas fa-file-invoice"></i>
                    </div>
                    <p class="bold">Планування та контроль різних витрат</p>
                    <!-- <p
                      class="light thin"
                      style="font-weight: 100; font-family: roboto;"
                    >
                      Короткий опис, який додатково розписує пункт в два рядки.
                    </p> -->
                </div>
                <!-- Item 3 -->
                <div class="col-6 col-sm-6 col-md-3 hiw-item mb-30">
                    <div class="bg-color-2 puls mb-30" style="animation-delay: 1.5s">
                        <i class="fas fa-file-invoice-dollar"></i>
                    </div>
                    <p class="bold">Значна економія коштів на 1 гектар</p>
                    <!-- <p
                      class="light thin"
                      style="font-weight: 100; font-family: roboto;"
                    >
                      Короткий опис, який додатково розписує пункт в два рядки.
                    </p> -->
                </div>
                <!-- Item 4 -->
                <div class="col-6 col-sm-6 col-md-3 hiw-item mb-30">
                    <div class="bg-color-2 puls mb-30" style="animation-delay: 2.25s">
                        <i class="fas fa-file-alt"></i>
                    </div>
                    <p class="bold">Зручна та сприйнятна звітність інвестору</p>
                    <!-- <p
                      class="light thin"
                      style="font-weight: 100; font-family: roboto;"
                    >
                      Короткий опис, який додатково розписує пункт в два рядки.
                    </p> -->
                </div>
            </div>
        </div>
    </section>
    <!-- How it works end -->
    <!-- Call to action -->
    <!-- <section class="call-to-action bg-animation p-90-60">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-6 align-self-center mb-30">
            <div class="section-title">
              <h2 class="h1 white">
                <span class="thin">Наша</span> <b>візія</b>
              </h2>
              <p class="white-light thin m-0 marg-r-0">
                Застосувати інноваційні технології для ефективного управління
                агрокомпанією для максимізація прибутків в реальному часі.
                Застосувати інноваційні технології для ефективного управління
                агрокомпанією для максимізація прибутків в реальному часі.
              </p>
            </div>
          </div>
          <div class="col-12 col-lg-6 align-self-center mb-30">
            <div class="section-title">
              <h2 class="h1 white">
                <span class="thin">Наша</span> <b>місія</b>
              </h2>
              <p class="white-light thin m-0 marg-r-0">
                Застосувати інноваційні технології для ефективного управління
                агрокомпанією для максимізація прибутків в реальному часі.
                Застосувати інноваційні технології для ефективного управління
                агрокомпанією для максимізація прибутків в реальному часі.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- Call to action end -->
    <!-- Features -->
    <!-- About left -->
    <section id="section-1" class="about-2 p-90-60">
        <div class="container" id="top">
            <div class="row">
                <!-- Text -->
                <div class="col-12 col-lg-5 mb-30 align-self-center">
                    <!-- Title -->
                    <div class="section-title mb-30">
                        <p class="light bold small">Що ми пропонуємо</p>
                        <h2><b>Оперативний облік</b></h2>
                    </div>

                    <p
                            data-id=1
                            class="p-blok"
                            data-s-img="https://agrocase.com/img/image1/image1_0_s.png"
                            data-b-img="https://agrocase.com/img/image1/image1_0_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span class="active-link-slider"> Типова конфігурація BAS ERP для України</span>
                    </p>

                    <p
                            data-id=2
                            class="p-blok"
                            data-s-img="https://agrocase.com/img/image1/image1_s.png"
                            data-b-img="https://agrocase.com/img/image1/image1_b.jpg"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>Технологічні карти та структура полів</span>
                    </p>
                    <p
                            data-id=3
                            class="p-blok"
                            data-s-img="https://agrocase.com/img/image1/image1_2_s.png"
                            data-b-img="https://agrocase.com/img/image1/image1_2_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>Паї (земельний банк)</span>
                    </p>
                    <p class="p-blok"
                       data-id=4
                       class="p-blok"
                       data-s-img="https://agrocase.com/img/image1/image1_3_s.png"
                       data-b-img="https://agrocase.com/img/image1/image1_3_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>Облік ТМЦ та робіт</span>
                    </p>
                    <p class="p-blok"
                       data-id=5
                       class="p-blok"
                       data-s-img="https://agrocase.com/img/image1/image1_4_s.png"
                       data-b-img="https://agrocase.com/img/image1/image1_4_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>ПММ (облік по тех операціям та техніці)</span>
                    </p>
                    <p class="p-blok"
                       data-id=6
                       class="p-blok"
                       data-s-img="https://agrocase.com/img/image1/image1_5_s.png"
                       data-b-img="https://agrocase.com/img/image1/image1_5_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>Ремонти (витрати на кожну одиницю техніки)</span>
                    </p>
                    <p class="p-blok"
                       data-id=7
                       class="p-blok"
                       data-s-img="https://agrocase.com/img/image1/image1_6_s.png"
                       data-b-img="https://agrocase.com/img/image1/image1_6_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>Збір урожаю</span>
                    </p>
                    <!--
                      <ul>
                          <li>Планування (технологічні карти)</li>
                          <li>Структуру посівних площ (історія поля)</li>
                      </ul>
                    -->
                    <!-- Text -->
                    <!--
                      <p class="light thin p-0-15">Dolor sit Mollitia harum ea ut eaque velit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus dignissimos aspernatur harum odio sit natus, sed molestiae, iste dolore vero nostrum velit voluptas delectus ullam exercitationem expedita voluptatum. Esse distinctio dolore nobis ex iure repellendus fuga. Excepturi tempora numquam odio, dolor velit repudiandae at enim totam autem, perferendis dolorum eaque?</p>
                    -->
                    <!-- Buttons -->
                    <a href="#." data-toggle="modal" data-target="#for-cost-modal">
                        <div class="button bg-color-1 cost-btn">Заявка на вартість</div>
                    </a>
                    <a
                            class="gray link present-btn"
                            href="#."
                            data-toggle="modal"
                            data-target="#for-present-modal"
                    >Заявка на презентацію <i class="fas fa-chevron-right"></i
                        ></a>
                </div>
                <!-- Ilustration -->
                <div class="col-12 col-md-6 align-self-center header-macbook jump">
                    <img src="img\ipad.png" alt="screens" />
                    <div class="img-screen">
                        <a class="img-b" data-fancybox="images1" href="https://agrocase.com/img/image1/image1_0_b.png">
                            <img class="img-s" src="https://agrocase.com/img/image1/image1_0_s.png" alt="" />
                        </a>

                        <a class="img-b" data-fancybox="images1" href="https://agrocase.com/img/image1/image1_s.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images1" href="https://agrocase.com/img/image1/image1_2_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images1" href="https://agrocase.com/img/image1/image1_3_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images1" href="https://agrocase.com/img/image1/image1_4_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images1" href="https://agrocase.com/img/image1/image1_5_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images1" href="https://agrocase.com/img/image1/image1_6_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About left end -->
    <!-- About right -->
    <section id="section-2" class="about-1 p-0-60">
        <div class="container" id="hiw">
            <div class="row">
                <!-- Text -->
                <div class="col-12 col-lg-5 order-lg-last mb-30 align-self-center">
                    <!-- Title -->
                    <div class="section-title mb-30">
                        <p class="light bold small">Що ми пропонуємо</p>
                        <h2><b>Мобільний додаток </b></h2>
                    </div>
                    <p class="light thin p-0-15 p-blok">
                        Мобільні додатки інтегруються з блоками оперативного обліку: облік робіт і ТМЦ, облік ПММ, ремонти (облік затрат в розрізі одиниць техніки)
                    </p>
                    <p class="p-blok"
                       data-id=1
                       class="p-blok"
                       data-s-img="https://agrocase.com/img/image2/image2_0_s.png"
                       data-b-img="https://agrocase.com/img/image2/image2_0_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>DASHBOARD КЕРІВНИКА</span>
                    </p>
                    <p class="p-blok"
                       data-id=2
                       class="p-blok"
                       data-s-img="https://agrocase.com/img/image2/image2_1_s.png"
                       data-b-img="https://agrocase.com/img/image2/image2_1_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>КПК АГРОНОМ </span>
                    </p>
                    <p class="p-blok"
                       data-id=3
                       class="p-blok"
                       data-s-img="https://agrocase.com/img/image2/image2_2_s.png"
                       data-b-img="https://agrocase.com/img/image2/image2_2_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>КПК МЕХАНІК </span>
                    </p>
                    <p class="p-blok"
                       data-id=4
                       class="p-blok"
                       data-s-img="https://agrocase.com/img/image2/image2_3_s.png"
                       data-b-img="https://agrocase.com/img/image2/image2_3_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>КПК ЗАПРАВНИК</span>
                    </p>

                    <a href="#." data-toggle="modal" data-target="#for-cost-modal">
                        <div class="button bg-color-1 cost-btn">Заявка на вартість</div>
                    </a>
                    <a
                            class="gray link present-btn"
                            href="#."
                            data-toggle="modal"
                            data-target="#for-present-modal"
                    >Заявка на презентацію <i class="fas fa-chevron-right"></i
                        ></a>
                </div>
                <!-- Ilustration -->
                <div class="col-12 col-md-7 align-self-center header-mob header-mob-left jump">
                    <img src="img\ipad.png" alt="screens" />
                    <div class="img-screen-left">
                        <a class="img-b" data-fancybox="images2" href="img/image2/image2_0_b.png">
                            <img class="img-s" src="img/image2/image2_0_s.png" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images2" href="img/image2/image2_1_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images2" href="img/image2/image2_2_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images2" href="img/image2/image2_3_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About right end -->
    <!-- About left -->


    <!-- <section id="section-3" class="about-2 p-0-60">
      <div class="container" id="about">
        <div class="row">
          <div class="col-12 col-lg-5 mb-30 align-self-center">
            <div class="section-title mb-30">
              <p class="light bold small">Що ми пропонуємо</p>
              <h2><b>Картографія та телематика</b></h2>
            </div>
            <p class="light thin p-0-15 p-block">
              Короткий опис блоку при необхідності. Ключові слова та
              роз'яснення в два чи три рядочки. Короткий опис блоку при
              необхідності. Ключові слова та роз'яснення в два чи три рядочки.
            </p>
            <p class="p-blok"
              data-id=1
              data-s-img="file:///D:/GOLD-FISH-NEW/agrocase/img/image3/image3_s.jpg"
              data-b-img="file:///D:/GOLD-FISH-NEW/agrocase/img/image3/image3_b.jpg"
            >
              <i class="fas fa-dot-circle"></i
              ><span class="active-link-slider">Карта полів по культурі</span>
            </p>
            <p class="p-blok"
              data-id=2
              data-s-img="file:///D:/GOLD-FISH-NEW/agrocase/img/image3/image3_2_s.jpg"
              data-b-img="file:///D:/GOLD-FISH-NEW/agrocase/img/image3/image3_2_b.jpg"
            >
              <i class="fas fa-dot-circle"></i
              ><span>Карта врожайності</span>
            </p>
            <p class="p-blok">
              <i class="fas fa-dot-circle"></i
              ><span
                >Моніторинг роботи техніки (інтеграція з РКС та вавілон)</span
              >
            </p>
            <a href="#." data-toggle="modal" data-target="#for-cost-modal">
              <div class="button bg-color-1 cost-btn">Заявка на вартість</div>
            </a>
            <a
              class="gray link present-btn"
              href="#."
              data-toggle="modal"
              data-target="#for-present-modal"
              >Заявка на презентацію <i class="fas fa-chevron-right"></i
            ></a>
          </div>

          <div class="col-12 col-md-6 align-self-center header-macbook jump">
            <img src="img\ipad.png" alt="screens" />

            <div class="img-screen">
              <a class="img-b" data-fancybox="images3" href="img/image3/image3_b.jpg">
                <img class="img-s" src="img/image3/image3_s.jpg" alt="" />
              </a>
              <a class="img-b" data-fancybox="images3" href="img/image3/image3_2_b.jpg">
                <img class="img-s" src="#" alt="" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </section> -->

    <section id="section-5" class="about-2">
        <div class="container" id="price">
            <div class="row">
                <!-- Text -->
                <div class="col-12 col-lg-5 mb-30 align-self-center">
                    <!-- Title -->
                    <div class="section-title mb-30">
                        <p class="light bold small">Що ми пропонуємо</p>
                        <h2><b>Фінансовий результат</b></h2>
                    </div>
                    <!-- Text -->
                    <p class="light thin p-0-15 p-blok">
                        Гнучкий інструмент ERP дозволяє порівнювати план-факторні показники доходів/витрат в різних сценаріях бюджетування та різних валютах.
                    </p>

                    <p class="p-blok"
                       data-id=1
                       data-s-img="https://agrocase.com/img/image5/image5_1_s.png"
                       data-b-img="https://agrocase.com/img/image5/image5_1_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span class="active-link-slider">План-фактний аналіз прямих витрат</span>
                    </p>
                    <p class="p-blok"
                       data-id=2
                       data-s-img="https://agrocase.com/img/image5/image5_2_s.png"
                       data-b-img="https://agrocase.com/img/image5/image5_2_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>Прибуток підприємства</span>
                    </p>
                    <!-- Buttons -->
                    <a href="#." data-toggle="modal" data-target="#for-cost-modal">
                        <div class="button bg-color-1 cost-btn">Заявка на вартість</div>
                    </a>
                    <a
                            class="gray link present-btn"
                            href="#."
                            data-toggle="modal"
                            data-target="#for-present-modal"
                    >Заявка на презентацію <i class="fas fa-chevron-right"></i
                        ></a>
                </div>
                <!-- Ilustration -->
                <div class="col-12 col-md-6 align-self-center header-macbook jump">
                    <img src="img\ipad.png" alt="screens" />
                    <div class="img-screen">
                        <a class="img-b" data-fancybox="images5" href="https://agrocase.com/img/image5/image5_1_b.png">
                            <img class="img-s" src="https://agrocase.com/img/image5/image5_1_s.png" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images5" href="https://agrocase.com/img/image5/image5_2_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- About left end -->
    <!-- About right -->
    <section id="testimonials" class="about-1 p-0-60">
        <div class="container" id="section-4">
            <div class="row">
                <!-- Text -->
                <div class="col-12 col-lg-5 order-lg-last mb-30 align-self-center">
                    <!-- Title -->
                    <div class="section-title mb-30">
                        <p class="light bold small">Що ми пропонуємо</p>
                        <h2><b>Аналітика (CEO-BI) </b></h2>
                    </div>
                    <!-- Text -->
                    <p class="p-blok"
                       data-id=1
                       data-s-img="https://agrocase.com/img/image4/image4_1_s.png"
                       data-b-img="https://agrocase.com/img/image4/image4_1_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span class="active-link-slider">Аналіз витрат на матеріали</span>
                    </p>
                    <p class="p-blok"
                       data-id=2
                       data-s-img="https://agrocase.com/img/image4/image4_2_s.png"
                       data-b-img="https://agrocase.com/img/image4/image4_2_b.png"
                    >
                        <i class="fas fa-dot-circle"></i><span>Контроль використання ПММ</span>
                    </p>
                    <p class="p-blok"
                       data-id=3
                       data-s-img="https://agrocase.com/img/image4/image4_3_s.png"
                       data-b-img="https://agrocase.com/img/image4/image4_3_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>Аналіз зарплати</span>
                    </p>
                    <p class="p-blok"
                       data-id=4
                       data-s-img="https://agrocase.com/img/image4/image4_4_s.png"
                       data-b-img="https://agrocase.com/img/image4/image4_4_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>Карта поля (історія)</span>
                    </p>
                    <p class="p-blok"
                       data-id=5
                       data-s-img="https://agrocase.com/img/image4/image4_5_s.png"
                       data-b-img="https://agrocase.com/img/image4/image4_5_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>Урожайність</span>
                    </p>
                    <p class="p-blok"
                       data-id=6
                       data-s-img="https://agrocase.com/img/image4/image4_6_s.png"
                       data-b-img="https://agrocase.com/img/image4/image4_6_b.png"
                    >
                        <i class="fas fa-dot-circle"></i
                        ><span>Динаміка цін по заморозці ягід </span>
                    </p>


                    <!--
                      <p class="p-blok"><i class="fas fa-dot-circle"></i><span>Контроль використання Добрива</span></p>

                      <p class="p-blok"><i class="fas fa-dot-circle"></i><span>Контроль використання ЗЗР</span></p>
                      <p class="p-blok"><i class="fas fa-dot-circle"></i><span>Контроль використання ПММ</span></p>
                    -->

                    <!-- Buttons -->
                    <a href="#." data-toggle="modal" data-target="#for-cost-modal">
                        <div class="button bg-color-1 cost-btn">Заявка на вартість</div>
                    </a>
                    <a
                            class="gray link present-btn"
                            href="#."
                            data-toggle="modal"
                            data-target="#for-present-modal"
                    >Заявка на презентацію <i class="fas fa-chevron-right"></i
                        ></a>
                </div>
                <!-- Ilustration -->
                <div class="col-12 col-md-7 align-self-center header-mob header-mob-left jump">
                    <img src="img\ipad.png" alt="screens" />
                    <div class="img-screen-left">
                        <a class="img-b" data-fancybox="images4" href="https://agrocase.com/img/image4/image4_1_b.png">
                            <img class="img-s" src="https://agrocase.com/img/image4/image4_1_s.png" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images4" href="https://agrocase.com/img/image4/image4_2_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images4" href="https://agrocase.com/img/image4/image4_3_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images4" href="https://agrocase.com/img/image4/image4_4_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images4" href="https://agrocase.com/img/image4/image4_5_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                        <a class="img-b" data-fancybox="images4" href="https://agrocase.com/img/image4/image4_6_b.png">
                            <img class="img-s" src="#" alt="" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About right end -->
    <!-- About left -->

    <section id="contact" class="about-2 p-0-60 section-arch">
        <div class="container" >
            <!-- Title -->
            <div class="section-title mb-30">
                <p class="light bold small">Що ми пропонуємо</p>
                <h2><b>Архітектура системи</b></h2>
            </div>
            <div class="row">
                <!-- Text -->
                <div class="col-12 col-lg-6 ">
                    <p class="light bold small mb-10">Схема підключення</p>
                    <div class="shema">
                        <img src="img/shema.png" alt="">
                    </div>
                </div>
                <!-- Ilustration -->
                <div class="col-12 col-lg-6 varworks">
                    <p class="light bold small mb-10">Варіанти роботи</p>

                    <div class="varWork">
                        <span class="varWork-count">1</span>
                        <p>Сервер асоціації, підключатись до якого можуть всі компанії</p>
                    </div>

                    <div class="varWork">
                        <span class="varWork-count">2</span>
                        <p>Сервер кооперативу, можуть об'єднатися члени кооперативу і мати свій сервер </p>
                    </div>

                    <div class="varWork">
                        <span class="varWork-count">3</span>
                        <p>Окремий сервер компанії</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About left end -->




    <!-- <section class="video bg-animation p-90-60 video-section">
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-7 align-self-center mb-30">
            <div class="section-title">
              <p class="white-light bold small">Ефективне рішення</p>
              <h2 class="h1 white">
                <span class="thin">Переглянути</span> <b>Відео</b>
                <span class="thin">Презентацію</span>
              </h2>
              <p class="white-light thin m-0">
                Заголовок, який спонукає переглянути відео
              </p>
            </div>
          </div>
          <div class="col-12 col-sm-5 align-self-center mb-30">
            <div class="play-frame play-rotate"></div>
            <a
              href="https://www.youtube.com/watch?v=i4klV22IAhk"
              data-lity=""
            >
              <div class="play-button bg-color-1">
                <i class="fas fa-play"></i>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section> -->




    <!-- Price -->
    <section class="pricing p-90-60">
        <div class="container">
            <!-- Section title -->
            <div class="section-title sm-ac mb-90">
                <p class="light bold small">Вже використовують систему</p>
                <h2><span class="thin">Наші </span><b>клієнти</b></h2>
            </div>
            <!-- Section title end -->
            <div class="row">
                <div class="col-12 col-lg-4 mb-30">
                    <div class="pricing-table text-center">
                        <div class="row">
                            <div class="col-12">
                                <p class="bold price-header" style="color:#7a9728">
                                    ТОВ<br />"СП АГРО-ОРГАНІК"
                                </p>
                            </div>
                            <div class="col-12">
                                <p class="light thin">Вирощування органічних ягід</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 mb-30">
                    <div class="pricing-table text-center">
                        <div class="row">
                            <div class="col-12">
                                <p class="bold price-header" style="color:#7a9728">
                                    ТОВ<br />"АГРО-ФРОСТ"
                                </p>
                            </div>
                            <div class="col-12">
                                <p class="light thin">Заморозка та переробка ягід.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 mb-30">
                    <div class="pricing-table text-center">
                        <div class="row">
                            <div class="col-12">
                                <p class="bold price-header" style="color:#7a9728">
                                    ТОВ<br />"СК ПРОМІНЬ"
                                </p>
                            </div>
                            <div class="col-12">
                                <p class="light thin">Вирощування зернових культур.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Price end -->

    <!-- Contact -->
    <section class="contact p-0-60">
        <div class="container">
            <div class="bg-map"><img src="img\bg-map.png" alt="map" /></div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <!-- Section title -->
                    <div class="section-title sm-ac mb-90">
                        <p class="light bold small">Зв'язатися з нами</p>
                        <h2><span class="thin">Контактна </span><b>інформація</b></h2>
                    </div>
                </div>
            </div>
            <!-- Section title end -->
            <div class="row phone-address-email">
                <div class="col-12 col-lg-4 cont-flex contact-f-1 jump">
                    <div class="contact-i">
                        <i class="fas fa-phone-volume"></i>
                    </div>
                    <span>+38-(067)-360-48-47</span>
                </div>
                <div class="col-12 col-lg-4 cont-flex contact-f-2 jump">
                    <div class="contact-i">
                        <i class="fas fa-map-marker-alt"></i>
                    </div>
                    <span>м. Рівне, вул. Соборна 16, приміщення Комп'ютерної Академії ШАГ</span>
                </div>
                <div class="col-12 col-lg-4 cont-flex contact-f-3 jump">
                    <div class="contact-i">
                        <i class="fas fa-envelope"></i>
                    </div>
                    <span>info@agrocase.com</span>
                </div>
            </div>
            <form id="form-section" action="{{ route('send_mail') }}" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-12 col-md">
                        <!-- Name input -->
                        <input id="home-form-name" type="text" name="name" placeholder="Імя *" />
                        <!-- Email input -->
                        <input
                                id="home-form-email"
                                type="email"
                                name="email"
                                placeholder="Електронна адреса"
                        />
                    </div>
                    <div class="col-12 col-md">
                        <!-- Text input -->
                        <textarea
                                id="home-form-message"
                                name="messages"
                                placeholder="Ваше повідомлення"></textarea>
                    </div>
                    <div class="col-12 col-md-auto text-center">
                        <!-- Submit form button -->
                        <button id="home-form-btn" class="submit bg-color-1">
                            <i class="fa fa-paper-plane" aria-hidden="true"></i>
                            <div class="spinner-load">
                                <div class="rect1"></div>
                                <div class="rect2"></div>
                                <div class="rect3"></div>
                                <div class="rect4"></div>
                                <div class="rect5"></div>
                            </div>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <!-- Popup "success" -->
        <div class="popup n-active-popup">
            <!-- Popup window -->
            <div class="popup-window">
                <!-- Popup icon -->
                <img src="img\email.png" alt="success" />
                <!-- title -->
                <p class="bold">Success</p>
                <!-- Text -->
                <p class="light thin">You have successfully sent us a message.</p>
                <!-- Button -->
                <button class="button bg-color-1">Well done!</button>
            </div>
            <!-- Popup window end -->
        </div>
        <!-- Popup end -->
    </section>
    <!-- Contact end -->
    <!-- Footer -->
    <!---->
    <!-- Footer end -->
    <!-- Copyright -->
    <div class="footer-copyright">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-12 col-sm-6">
                    <!-- Copyright -->
                    <p class="small thin white-light sm-ac">
                        Copyrights © 2018. All Rights Reserved.
                    </p>
                </div>
                <div class="col-12 col-sm-6">
                    <!-- Author -->
                    <p class="small thin white-light author sm-ac">
                        Designed by
                        <a href="https://goldfish-web.com/">Web-Studio Gold Fish</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Copyright -->
</div>
<!-- Page end -->

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<!-- Bootstrap -->
<script src="/js/bootstrap.min.js"></script>
<!-- ajax -->
{{--<script src="/js/ajax.js"></script>--}}
<!-- Smooth Scroll -->
<script src="/js/smooth-scroll.min.js"></script>
<!-- Owl Carousel -->
<script src="/js/owl.carousel.min.js"></script>
<!-- Lity -->
<script src="/js/lity.js"></script>
<!-- Main -->

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/section.js"></script>
<script src="/js/contact.js"></script>

</body>
</html>
