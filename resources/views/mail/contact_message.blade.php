<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title></title>
</head>
<body>
<table cellpadding="0" cellspacing="0" >
    <h1>Зворотній зв'язок!</h1>
    <tfoot style="font-size: 13pt;">
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td style="padding: 5px 25px 0">Ім'я: </td>
                    <td style="padding: 5px 25px 0;color: #510D00;"><b>{{$messageName}}</b></td>
                </tr>
                <tr>
                    <td style="padding: 5px 25px 0">Електронна пошта: </td>
                    <td style="padding: 5px 25px 0;color: #510D00;"><b>{{ $messageEmail }}</b></td>
                </tr>
                <tr>
                    <td style="padding: 5px 25px 0">Повідомлення: </td>
                    <td style="padding: 5px 25px 0;color: #510D00;"><b>{{ $messageMessages }}</b></td>
                </tr>
                <tr>
                    <td style="padding: 20px 25px 15px;">Дата заявки: </td>
                    <td style="padding: 20px 25px 15px;color: #510D00;"><b>{{ $messageDate->toDateTimeString() }}</b></td>
                </tr>
            </table>
        </td>
    </tr>
    </tfoot>
</table>
</body>
</html>

