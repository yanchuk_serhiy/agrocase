<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' , 'HomeController@index');

Route::post('send_mail', 'ContactMailController@send')->name('send_mail');

Route::post('present_send', 'PresentMessageController@send')->name('present_send');

Route::post('vartist_send', 'VartistMessageController@send')->name('vartist_send');


