$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(".phone-valid").keypress(function (e) {
    if (e.which != 43 && e.which != 41 && e.which != 40 && e.which != 46 && e.which != 45 && e.which != 46 &&
        !(e.which >= 48 && e.which <= 57)) {
        return false;
    }
});

$(".user-count").keypress(function (e) {
    if (e.which != 43 && e.which != 41 && e.which != 40 && e.which != 46 && e.which != 45 && e.which != 46 &&
        !(e.which >= 48 && e.which <= 57)) {
        return false;
    }
});


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}



$('#cost-form input').on('focus', function () {
    $(this).removeClass('incorect-input')
})

$('#present-form input').on('focus', function () {
    $(this).removeClass('incorect-input')
})


// COST FORM !!!!!!!!!!!

$('#cost-submit').on('click', function (e) {
    e.preventDefault();

    // console.log('COST FORM')

    let name = $('#cost-name').val();
    let phone = $('#cost-phone').val();
    let email = $('#cost-email').val();
    let company = $('#cost-company').val();
    let how_user = $('#cost-user-count').val();
    let region =$('#cost-oblast').val();

    let messages = $('#cost-messege').val();

    let saveEmail = $('#cost-email').val();

    let costError = false;

    $('#cost-email').on('focus', function () {
        $('#cost-email').attr('placeholder', '');

        if(saveEmail == ''){
            $('#cost-email').attr('placeholder', 'Електронна адреса *');
        }else{
            $('#cost-email').val(saveEmail);
        } 
    })

    $('#cost-name').on('focus', function () {
        $(this).attr('placeholder', 'Імя *');
    })

    $('#cost-phone').on('focus', function () {
        $(this).attr('placeholder', 'Телефон *');
    })

    $('#cost-company').on('focus', function () {
        $(this).attr('placeholder', 'Компанія *');
    })

    


    if(name == ''){
        $('#cost-name').val('');
        $('#cost-name').attr('placeholder', 'Обов\'язкове поле');
        $('#cost-name').addClass('incorect-input');
        costError = true;
    }

    if(phone == ''){
        $('#cost-phone').val('');
        $('#cost-phone').attr('placeholder', 'Обов\'язкове поле');
        $('#cost-phone').addClass('incorect-input');
        costError = true;
    }

    if(company == ''){
        $('#cost-company').val('');
        $('#cost-company').attr('placeholder', 'Обов\'язкове поле');
        $('#cost-company').addClass('incorect-input');
        costError = true;
    }

    if(email == ''){
        $('#cost-email').val('');
        $('#cost-email').attr('placeholder', 'Обов\'язкове поле');
        $('#cost-email').addClass('incorect-input');
        costError = true;
    }


    if (!validateEmail(email)) {
        $('#cost-email').val('');
        $('#cost-email').attr('placeholder', 'Невірний формат');
        $('#cost-email').addClass('incorectEmail');
        $('#cost-email').addClass('incorect-input');
        costError = true;
    }


    if(costError==false){

        $('#for-cost-modal').modal('hide')

        $.ajax({
            url: 'vartist_send',
            type: 'post',
            // contentType: false,      
            // cache: false,       
            // processData: false,
            data: {
                name: name,
                phone: phone,
                email: email,
                company: company,
                how_user:how_user,
                region: region,
                messages: messages
            },
            success: function (data) {
                if (data.status == 'success') {
                    
                    $('#cost-form input').val('');
                    $('#cost-form textarea').val('');

                    $('.custom-select .selected-option span').html('Область');

                    $("#cost-oblast :selected").removeAttr("selected");

                    saveEmail = '';

                    $('#cost-name').attr('placeholder', 'Ім`я *');
                    $('#cost-phone').attr('placeholder', 'Телефон *');
                    $('#cost-email').attr('placeholder', 'Електронна адреса *');
                    $('#cost-company').attr('placeholder', 'Компанія *');
                    $('#cost-user-count').attr('placeholder', 'Кількість користувачів');
                    $('#cost-oblast').attr('placeholder', 'Область');
                    $('.selected-option').removeClass('activeselect')
                    $('#cost-messege').attr('placeholder', 'Вид діяльності компанії та блоки які вас цікавлять');
                    

                    setTimeout(function(){
                        $('#success-modal').modal('show')
                    }, 500);

                }

            },
            error: function (error) {
                console.log(error);
            }
        });

        // console.log(name, phone, email, company, users, messege, oblast)

        // $('#cost-form input').val('');
        // $('#cost-form textarea').val('');

        // $('.custom-select .selected-option span').html('Область');

        // $("#cost-oblast :selected").removeAttr("selected");

        // saveEmail = '';

        // $('#cost-name').attr('placeholder', 'Ім`я *');
        // $('#cost-phone').attr('placeholder', 'Телефон *');
        // $('#cost-email').attr('placeholder', 'Електронна адреса *');
        // $('#cost-company').attr('placeholder', 'Компанія *');
        // $('#cost-user-count').attr('placeholder', 'Кількість користувачів');
        // $('#cost-oblast').attr('placeholder', 'Область');
        // $('.selected-option').removeClass('activeselect')
        // $('#cost-messege').attr('placeholder', 'Вид діяльності компанії та блоки які вас цікавлять');


        // $('#for-cost-modal').modal('hide')

        // setTimeout(function(){
        //     $('#success-modal').modal('show')
        // }, 500);
        
    }

})


// COST FORM END   !!!!!!!!!!!





// ----------------------- PRESENT FORM

$('#present-submit').on('click', function (e) {
    e.preventDefault();

    // console.log('present FORM')

    let name = $('#present-name').val();
    let phone = $('#present-phone').val();
    let email = $('#present-email').val();
    let company = $('#present-company').val();
    let region =$('#present-oblast').val();

    let messages = $('#present-messege').val();

    let saveEmail = $('#present-email').val();

    let presentError = false;

    $('#present-email').on('focus', function () {
        $('#present-email').attr('placeholder', '');

        if(saveEmail == ''){
            $('#present-email').attr('placeholder', 'Електронна адреса *');
        }else{
            $('#present-email').val(saveEmail);
        } 
    })

    $('#present-name').on('focus', function () {
        $(this).attr('placeholder', 'Імя *');
    })

    $('#present-phone').on('focus', function () {
        $(this).attr('placeholder', 'Телефон *');
    })

    $('#present-company').on('focus', function () {
        $(this).attr('placeholder', 'Компанія *');
    })

    


    if(name == ''){
        $('#present-name').val('');
        $('#present-name').attr('placeholder', 'Обов\'язкове поле');
        $('#present-name').addClass('incorect-input');
        presentError = true;
    }

    if(phone == ''){
        $('#present-phone').val('');
        $('#present-phone').attr('placeholder', 'Обов\'язкове поле');
        $('#present-phone').addClass('incorect-input');
        presentError = true;
    }

    if(company == ''){
        $('#present-company').val('');
        $('#present-company').attr('placeholder', 'Обов\'язкове поле');
        $('#present-company').addClass('incorect-input');
        presentError = true;
    }

    if(email == ''){
        $('#present-email').val('');
        $('#present-email').attr('placeholder', 'Обов\'язкове поле');
        $('#present-email').addClass('incorect-input');
        presentError = true;
    }


    if (!validateEmail(email)) {
        $('#present-email').val('');
        $('#present-email').attr('placeholder', 'Невірний формат');
        $('#present-email').addClass('incorectEmail');
        $('#present-email').addClass('incorect-input');
        presentError = true;
    }


    if(presentError==false){

        $('#for-present-modal').modal('hide')

    
        $.ajax({
            url: 'present_send',
            type: 'post',
            // contentType: false,      
            // cache: false,       
            // processData: false,
            data: {
                name: name,
                phone: phone,
                email: email,
                company: company,
                region: region,
                messages: messages
            },
            success: function (data) {
                if (data.status == 'success') {
                    
                    $('#present-form input').val('');
                    $('#present-form textarea').val('');

                    saveEmail = '';

                    $('#present-name').attr('placeholder', 'Ім`я *');
                    $('#present-phone').attr('placeholder', 'Телефон *');
                    $('#present-email').attr('placeholder', 'Електронна адреса *');
                    $('#present-company').attr('placeholder', 'Компанія *');
                    $('#present-oblast').attr('placeholder', 'Область');
                    $('.selected-option').removeClass('activeselect')
                    $('#present-messege').attr('placeholder', 'Вид діяльності компанії та блоки які вас цікавлять');
                    

                      setTimeout(function(){
                            $('#success-modal').modal('show')
                        }, 500);

                }

            },
            error: function (error) {
                console.log(error);
            }
        });


        // console.log(name, phone, email, company, messege, oblast)

        // $('#present-form input').val('');
        // $('#present-form textarea').val('');

        // saveEmail = '';

        // $('#present-name').attr('placeholder', 'Ім`я *');
        // $('#present-phone').attr('placeholder', 'Телефон *');
        // $('#present-email').attr('placeholder', 'Електронна адреса *');
        // $('#present-company').attr('placeholder', 'Компанія *');
        // $('#present-oblast').attr('placeholder', 'Область');
        // $('.selected-option').removeClass('activeselect')
        // $('#present-messege').attr('placeholder', 'Вид діяльності компанії та блоки які вас цікавлять');

        // setTimeout(function(){
        //     $('#success-modal').modal('show')
        // }, 500);
    }

})






/* ----------------- GENERAL HOME CONTACT FORM --------- */

$('#form-section input').on('focus', function () {
    $(this).removeClass('incorect-input')
})

$('#home-form-name').on('focus', function () {
    $(this).attr('placeholder', 'Імя *');
})

$('#home-form-btn').on('click', function (e) {
    e.preventDefault();

    let name = $('#home-form-name').val();
    let email = $('#home-form-email').val();
    let messages = $('#home-form-message').val();

    let saveEmail = $('#home-form-email').val();

    let homeError = false;

    $('#home-form-email').on('focus', function () {
        $('#home-form-email').attr('placeholder', '');

        if(saveEmail == ''){
            $('#home-form-email').attr('placeholder', 'Електронна адреса *');
        }else{
            $('#home-form-email').val(saveEmail);
        } 
    })

    if(name == ''){
        $('#home-form-name').val('');
        $('#home-form-name').attr('placeholder', 'Обов\'язкове поле');
        $('#home-form-name').addClass('incorect-input');
        homeError = true;
    }

    if (!validateEmail(email)) {
        $('#home-form-email').val('');
        $('#home-form-email').attr('placeholder', 'Невірний формат');
        $('#home-form-email').addClass('incorectEmail');
        $('#home-form-email').addClass('incorect-input');
        homeError = true;
    }

    if(homeError == false){
        // console.log(name, email, messages)
        $("#home-form-btn").attr("disabled", true);
        $('#home-form-btn i').hide();
        $('#home-form-btn .spinner-load').show();

        $('#form-section input').val('');
        $('#form-section textarea').val('');

        $('#home-form-name').attr('placeholder', 'Ім`я *');
        $('#home-form-email').attr('placeholder', 'Телефон *');
        $('#home-form-message').attr('placeholder', 'Ваше повідомлення');


        $.ajax({
            type: 'POST',
            url: 'send_mail',
            // contentType: false,      
            cache: false,
            // processData: false,
            data: {
                name: name,
                email: email,
                messages: messages
            },
            success: function (data) {
                if (data.status == 'success') {

                    saveEmail = '';
                    $("#home-form-btn").attr("disabled", false);
                    $('#home-form-btn .spinner-load').hide();
                    $('#home-form-btn i').show();
                   
                    
                    setTimeout(function(){
                        $('#success-modal').modal('show')
                    }, 500);

                }

            },
            error: function (error) {
                console.log(error);
            }
        });
    }

})