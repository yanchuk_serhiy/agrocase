/* -------------------------------------------

Name: 		App Showcase
Author:		Nazar Miller (millerDigitalDesign)
Portfolio:  https://themeforest.net/user/millerdigitaldesign/portfolio?ref=MillerDigitalDesign

p.s. I am available for Freelance hire (UI design, web development). mail: miller.themes@gmail.com

------------------------------------------- */
function customSelect(el){
    var options = [],
    option = $(el).children('option'),
    customSelect;

$(el).hide(); // hide select

// Create custom select
$(option).each(function(){ 
    options.push($(this).html());
});

$(el).after('<ul class="custom-select" data-selected-value="' + options[0] + '">');
customSelect = $(el).siblings('.custom-select');
$(customSelect).append('<li class="selected-option"><span>' + options[0] + '</span>');
$(customSelect).children('.selected-option').append('<ul class="options">');

for(var i = 1; i < options.length; i++) {
    $(customSelect).find('.options').append('<li data-value=' + options[i] + '>' + options[i] + '</li>');
}

// Click action & synchronization with origin select for submitting form     
$(customSelect).click(function(){
    $(this).toggleClass('open');
    $('.options',this).toggleClass('open');
});

$(customSelect).find('.options li').click(function(){
    var selection = $(this).text();
    var dataValue = $(this).attr('data-value');
    var selected = $(customSelect).find('.selected-option span').text(selection);
    for(var i = 1; i < option.length; i++) {
        if($(option[i]).text() === selected.text()) {
            $(option[i]).attr('selected', 'true');
            $(option[i]).siblings().removeAttr('selected');
        }
    };

    $(customSelect).attr('data-selected-value',dataValue);
});
};


customSelect('.custom-sel');

customSelect('.custom-sel-present');


$('#present-form .selected-option').on('click', function () {
    $('#present-form .selected-option').addClass('activeselect')
})

$('#cost-form .selected-option').on('click', function () {
    $('#cost-form .selected-option').addClass('activeselect')
})



function testAnim(x) {
    $('.modal .modal-dialog').attr('class', 'modal-dialog  ' + x + '  animated faster');
};
console.log($('.modal .modal-dialog'));
$('.modal').on('show.bs.modal', function (e) {
    console.log('SHOW')
    var anim = 'zoomIn'
    testAnim(anim);
})
$('.modal').on('hide.bs.modal', function (e) {
    console.log('HIDE')
  var anim = 'zoomOut';
      testAnim(anim);
})

$('[data-fancybox="images1"]').fancybox({
    loop: true,
    buttons: [
        "zoom",
        // "share",
        "slideShow",
        "fullScreen",
        // "download",
        "thumbs",
        "close"
      ],
      clickContent: function(current, event) {
        return current.type === "image" ? "zoom" : true;
      },
});

$('[data-fancybox="images3"]').fancybox({
    loop: true,
    buttons: [
        "zoom",
        // "share",
        "slideShow",
        "fullScreen",
        // "download",
        "thumbs",
        "close"
      ],
      clickContent: function(current, event) {
        return current.type === "image" ? "zoom" : true;
      },
});

$('[data-fancybox="images4"]').fancybox({
    loop: true,
    buttons: [
        "zoom",
        // "share",
        "slideShow",
        "fullScreen",
        // "download",
        "thumbs",
        "close"
      ],
      clickContent: function(current, event) {
        return current.type === "image" ? "zoom" : true;
      },
});

$('[data-fancybox="images5"]').fancybox({
    loop: true,
    buttons: [
        "zoom",
        // "share",
        "slideShow",
        "fullScreen",
        // "download",
        "thumbs",
        "close"
      ],
      clickContent: function(current, event) {
        return current.type === "image" ? "zoom" : true;
      },
});

//color switcher
$(document).ready(function () {
    // Color switcher
    $('.cog').on('click', function () {
        $('#color-sw').toggleClass('active-switcher');
    });
});

(function ($j) {
    switch_style = {
        onReady: function () {
            this.switch_style_click();
        },
        switch_style_click: function () {
            $(".box").click(function () {
                var id = $(this).attr("id");

                $("#switch_style").attr("href", "css/color-theme/" + id + ".css");
            });
        },
    };
    $j().ready(function () {
        switch_style.onReady();
    });

})(jQuery);

$(function () {

    "use strict";
    
    //preloader
    $(window).on('load', function () {
        $(".status").fadeOut();
        $(".preloader").delay(500).fadeOut("slow");
    })

    //navigation scrollspy
    var lastId,
        topMenu = $("#navigation"),
        topMenuHeight = topMenu.outerHeight() - 0,
        menuItems = topMenu.find("a"),
        scrollItems = menuItems.map(function () {
            var item = $($(this).attr("href"));
            if (item.length) {
                return item;
            }
        });

    //scrollspy smooth scroll 
    menuItems.on("click", function (e) {
        var href = $(this).attr("href"),
            offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight + 1;
        //default speed: 1200
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, 1200);
        e.preventDefault();


        if (window.innerWidth < 991) {
            $('.navbar-toggler').click();
        }
    });


    //Active nav link
    $(window).on("scroll", function () {
        var fromTop = $(this).scrollTop() + topMenuHeight;
        var cur = scrollItems.map(function () {
            if ($(this).offset().top < fromTop)
                return this;
        });
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";
        if (lastId !== id) {
            lastId = id;
            menuItems
                .parent().removeClass("active")
                .end().filter("[href='#" + id + "']").parent().addClass("active");
        }

        //navbar color after scroll
        var scroll = $(window).scrollTop();

        if (scroll >= 60) {
            $(".navbar").addClass("bg-scroll");
        } else {
            $(".navbar").removeClass("bg-scroll");
        }
    });

    // Testimonials slider
    $("#owl").owlCarousel({
        nav: true,
        loop: true,
        dots: false,
        navSpeed: 1000,
        navContainer: '.nav-container',
        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }
        }

    });

    // brands slider
    $('.brands-slider').owlCarousel({
        loop: true,
        nav: true,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplaySpeed: 2000,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });

    // close popup
    $(".popup").on('click', function () {
        $(".popup").removeClass("active-popup");
        $(".popup").addClass("n-active-popup");
    });

})(jQuery);


