let DOM = $('body');

DOM.on('click', '#section-1 p.p-blok', function(){

    $('#section-1 p.p-blok span').removeClass('active-link-slider');
    $(this).find('span').addClass('active-link-slider');

    let index = $(this).attr('data-id');

    let imgSSRC = $(this).attr('data-s-img');
    let imgSection = $('#section-1 .img-screen');
    let imgS = $(imgSection.find('.img-s')[index-1]);

    // console.log(imgS);

    imgSection.find('.img-s').parent().hide();
    imgS.parent().show();
    imgS.attr('src', imgSSRC);

})

DOM.on('click', '#section-2 p.p-blok', function(){

    $('#section-2 p.p-blok span').removeClass('active-link-slider');
    $(this).find('span').addClass('active-link-slider');

    let index = $(this).attr('data-id');

    let imgSSRC = $(this).attr('data-s-img');
    let imgSection = $('#section-2 .img-screen-left');
    let imgS = $(imgSection.find('.img-s')[index-1]);

    // console.log(imgS);

    imgSection.find('.img-s').parent().hide();
    imgS.parent().show();
    imgS.attr('src', imgSSRC);

})


DOM.on('click', '#section-3 p.p-blok', function(){

    $('#section-3 p.p-blok span').removeClass('active-link-slider');
    $(this).find('span').addClass('active-link-slider');

    let index = $(this).attr('data-id');

    let imgSSRC = $(this).attr('data-s-img');
    let imgSection = $('#section-3 .img-screen');
    let imgS = $(imgSection.find('.img-s')[index-1]);

    console.log(this);

    imgSection.find('.img-s').parent().hide();
    imgS.parent().show();
    imgS.attr('src', imgSSRC);

})


DOM.on('click', '#section-4 p.p-blok', function(){

    $('#section-4 p.p-blok span').removeClass('active-link-slider');
    $(this).find('span').addClass('active-link-slider');

    let index = $(this).attr('data-id');

    let imgSSRC = $(this).attr('data-s-img');
    let imgSection = $('#section-4 .img-screen-left');
    let imgS = $(imgSection.find('.img-s')[index-1]);

    // console.log(imgS);

    imgSection.find('.img-s').parent().hide();
    imgS.parent().show();
    imgS.attr('src', imgSSRC);

})

DOM.on('click', '#section-5 p.p-blok', function(){

    $('#section-5 p.p-blok span').removeClass('active-link-slider');
    $(this).find('span').addClass('active-link-slider');

    let index = $(this).attr('data-id');

    let imgSSRC = $(this).attr('data-s-img');
    let imgSection = $('#section-5 .img-screen');
    let imgS = $(imgSection.find('.img-s')[index-1]);

    // console.log(imgS);

    imgSection.find('.img-s').parent().hide();
    imgS.parent().show();
    imgS.attr('src', imgSSRC);

})
