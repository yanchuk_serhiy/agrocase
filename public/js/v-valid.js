$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

if (document.getElementById('test_form')) {

    new Vue({
        el: '#test_form',
        data: {
            name: '',
            email: '',
            msg: ''
        },
        mounted: function () {
            // Class for error styling
            var IncorrectFieldClass = "incorrect-field";
            // Required field message
            var RequiredFieldText = "required field";
            // Incorrect field message
            var IncorrectFieldText = "incorrect data entered";

            this.ValidatingInputName = new RegExValidatingInput($('[data-name]'), {
                expression: RegularExpressions.NAME,
                ChangeOnValid: function (input) {
                    input.removeClass(IncorrectFieldClass);
                },
                ChangeOnInvalid: function (input) {
                    input.addClass(IncorrectFieldClass);
                },
                showErrors: true,
                requiredErrorMessage: RequiredFieldText,
                regExErrorMessage: IncorrectFieldText
            });

            this.ValidatingInputEmail = new RegExValidatingInput($('[data-email]'), {
                expression: RegularExpressions.EMAIL,
                ChangeOnValid: function (input) {
                    input.removeClass(IncorrectFieldClass);
                },
                ChangeOnInvalid: function (input) {
                    input.addClass(IncorrectFieldClass);
                },
                showErrors: true,
                requiredErrorMessage: RequiredFieldText,
                regExErrorMessage: IncorrectFieldText
            });

            this.ValidatingInputText = new RegExValidatingInput($('[data-msg]'), {
                expression: RegularExpressions.MIN_TEXT,
                ChangeOnValid: function (input) {
                    input.removeClass(IncorrectFieldClass);
                },
                ChangeOnInvalid: function (input) {
                    input.addClass(IncorrectFieldClass);
                },
                showErrors: true,
                requiredErrorMessage: RequiredFieldText,
                regExErrorMessage: IncorrectFieldText
            });
        },

        methods: {
            sendforms: function () {
                var _this = this;

                var isValid = true;

                this.ValidatingInputName.Validate();
                if (!this.ValidatingInputName.isValid) {
                    isValid = false;
                }

                this.ValidatingInputEmail.Validate();
                if (!this.ValidatingInputEmail.isValid) {
                    isValid = false;
                }

                this.ValidatingInputText.Validate();
                if (!this.ValidatingInputText.isValid) {
                    isValid = false;
                }

                if (isValid) {
                    $.ajax({
                        type: 'POST',
                        url: 'send_mail',
                        cache: false,
                        data: {
                            name: _this.name,
                            email: _this.email,
                            msg: _this.msg,
                        },
                        success: function(result){

                            console.log(result);
                            $('#message').modal('show');
                            jQuery('#test_form')[0].reset();
                        },
                        error: function (error) {
                            console.log(error);
                        },
                        beforeSend: function () {
                            _this.isSending = true;
                        },
                        complete: function () {
                            _this.isSending = false;
                        }
                    });
                }
            }
        }
    });
}