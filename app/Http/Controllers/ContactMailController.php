<?php

namespace App\Http\Controllers;

use App\Mail\ContactMessageMail;
use App\Models\ContactMessageModel;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Mockery\CountValidator\Exception;
use Illuminate\Http\Request;

class ContactMailController extends Controller
{
    public function send (){
        $message = new ContactMessageModel();
        $message->name = request('name');
        $message->email = request('email');
        $message->messages = request('messages');

        if (!$message->save()) {
            return Response::json([
                'status' => 'error'
            ]);
        }

        try {
            Mail::to(config('mail.from.address'))
                ->send(new ContactMessageMail($message));
        } catch (Exception $e) {
//            \Debugbar::info("Не вдалося надіслати повідомлення: " . $e->getMessage());
        }

        return response()->json([
            'status' => 'success'
        ]);
    }
}
