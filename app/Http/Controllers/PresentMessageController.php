<?php

namespace App\Http\Controllers;

use App\Mail\PresentMessageMail;
use App\Models\PresentMessageModel;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Mockery\CountValidator\Exception;
use Illuminate\Http\Request;

class PresentMessageController extends Controller
{
    public function send (){
        $message = new PresentMessageModel();
        $message->name = request('name');
        $message->phone = request('phone');
        $message->email = request('email');
        $message->company = request('company');
        $message->region = request('region');
        $message->messages = request('messages');

        if (!$message->save()) {
            return Response::json([
                'status' => 'error'
            ]);
        }

        try {
            Mail::to(config('mail.from.address'))
                ->send(new PresentMessageMail($message));
        } catch (Exception $e) {
//            \Debugbar::info("Не вдалося надіслати повідомлення: " . $e->getMessage());
        }

        return response()->json([
            'status' => 'success'
        ]);
    }
}
