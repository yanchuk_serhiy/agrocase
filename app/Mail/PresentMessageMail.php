<?php

namespace App\Mail;

use App\Models\PresentMessageModel;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PresentMessageMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PresentMessageModel $message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Заявка на презентацію!")
            ->view('mail.present_message')
            ->with([
                'messageName' => $this->message->name,
                'messagePhone' => $this->message->phone,
                'messageEmail' => $this->message->email,
                'messageCompany' => $this->message->company,
                'messageRegion' => $this->message->region,
                'messageMessages' => $this->message->messages,
                'messageDate'  => $this->message->created_at
            ]);    }
}
