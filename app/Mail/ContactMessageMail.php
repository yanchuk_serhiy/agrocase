<?php

namespace App\Mail;

use App\Models\ContactMessageModel;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMessageMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactMessageModel $message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Зворотній зв'язок!")
            ->view('mail.contact_message')
            ->with([
                'messageName' => $this->message->name,
                'messageEmail' => $this->message->email,
                'messageMessages' => $this->message->messages,
                'messageDate'  => $this->message->created_at
            ]);    }
}
