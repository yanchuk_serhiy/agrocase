<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PresentMessageModel extends Model
{
    public $table = 'present_message';
}
